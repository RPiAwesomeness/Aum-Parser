use aum_lexer::lexer::{Lexer, Token};
use aum_lexer::tokens::TokenType;

pub fn parse_source(source: &mut String) -> Vec<Token> {
    let mut tokens: Vec<Token> = Vec::new();
    let mut lexer = Lexer::new(source);

    loop {
        let tok = lexer.next_token();
        if tok.token == TokenType::EndOfFile {
            break;
        }
        tokens.push(tok);
    }

    tokens
}
