use aum_lexer::tokens::TokenType;
use std::{fmt, iter, slice};
use types;

macro_rules! unimplemented {
    () => {
        panic!("UNIMPLEMENTED");
    };
}

/// Matches a token of type $token, returning the value matched
///
/// # Usage
///
/// ## Expect 1 type
/// expect!(iterator, TokenType::Semicolon = "terminator");
///
/// ## Expect one of 2 different types
/// expect!(iterator, [TokenType::Integer = "literal integer", TokenType::String = "literal string"]);
macro_rules! expect {
    ($iterator:ident, ($token:expr, $expected:expr)) => {{
        expect!($iterator, [($token, $expected)])
    }};

    ($iterator:ident,[$(($token:expr, $expected:expr)),+]) => {{
        // Generate expected tokens list
        let expected_tokens = vec![$(($token, $expected)),*];

        let next_token = $iterator
            .next()
            .expect("No further tokens available!");
            // .expect(format!("No further tokens available! Expected one of {:?}", expected_tokens).as_str());

        match *next_token {
            $(
                $token => Ok(val),
            )*
            _ => Err(format!("Unexpected token {:?}", *next_token).as_str()),
            // _ => Err(format!("Expected one of {:?}, got {:?}", expected_tokens, *next_token).as_str()),
        }
    }};
}

/// Matches a token of type $token, discarding anything related to it
///
/// # Usage
///
/// ## Discard 1 type
/// discard!(iterator, TokenType::Semicolon = "terminator");
///
/// ## Discard one of 2 different types
/// discard!(iterator, [TokenType::Integer = "literal integer", TokenType::String = "literal string"]);
macro_rules! discard {
    ($iterator:ident, ($token:expr, $expected:expr)) => {
        discard!($iterator, [($token, $expected)]);
    };

    ($iterator:ident,[$(($token:expr, $expected:expr)),+]) => {
        expect!($iterator, [$(($token, $expected)),*]);
    };
}

/// Ignores the next n tokens
///
/// # Usage
///
/// ## Ignore next 2 tokens
/// ignore!(iterator, 2);
macro_rules! ignore {
    ($iterator:ident, $n:expr) => {
        for i in 0..$n {
            $iterator.next().expect(
                format!(
                    "No further tokens available. Ignoring token {} of {}",
                    i, $n
                )
                .as_str(),
            );
        }
    };
}

macro_rules! terminator {
    ($iter:ident) => {
        discard!($iter, (TokenType::Semicolon, "terminator"));
    };
}

// Type alias for peekable Iterator over TokenTypes
type TokenTypeIter<'a> = iter::Peekable<slice::Iter<'a, TokenType>>;

// Type alias for resulting of statement parsing from tokens
type ParseResult = Result<Box<Statement>, &'static str>;

// C++14 NOP define to create the nop statement
const CXX_NOP_DEFINE: &'static str = "#define nop";

pub trait Statement: fmt::Debug {
    /// Generates the appropriate x86 assembly statement(s)
    fn gen_statement_asm_x86(&self) -> Vec<&'static str>;

    /// Generates the appropriate C++14 statement(s)
    fn gen_statement_cxx14(&self) -> Vec<&'static str>;

    /// Resolves whether the current state of the iterator passed is a valid statement
    fn parse(&self, initial: TokenType, iterator: TokenTypeIter<'_>) -> ParseResult;
}

#[derive(Debug)]
pub struct InvalidStatement {}

impl Statement for InvalidStatement {
    fn gen_statement_asm_x86(&self) -> Vec<&'static str> {
        vec!["nop"]
    }

    fn gen_statement_cxx14(&self) -> Vec<&'static str> {
        vec!["nop"]
    }

    fn parse(&self, initial: TokenType, iterator: TokenTypeIter<'_>) -> ParseResult {
        Err("INVALID STATEMENT")
    }
}

#[derive(Debug)]
pub struct Initialize<T>
where
    T: types::Type,
{
    pub val_type: T,
    pub identifier: String,
}

impl<T> Statement for Initialize<T>
where
    T: types::Type,
{
    fn gen_statement_asm_x86(&self) -> Vec<&'static str> {
        unimplemented!();
    }

    fn gen_statement_cxx14(&self) -> Vec<&'static str> {
        let assign_type = format!("int {} = {};", self.identifier, self.val_type.as_str()).as_str();

        vec![assign_type]
    }

    fn parse(&self, initial: TokenType, iterator: TokenTypeIter<'_>) -> ParseResult {
        let TokenType::Ident(ident) = initial;

        // Started with identifier, expect initialization operator
        discard!(
            iterator,
            (TokenType::Initialize, "initialization operator (:=)")
        );

        // Match against literal value
        let value: String = expect!(iterator, (TokenType::Integer(val), "integer literal"))?;

        // Expect terminating semicolon
        terminator!(iterator);

        Ok(Box::new(Initialize {
            identifier: ident,
            val_type: value,
        }))
    }
}

/// A return statement returning a variable
#[derive(Debug)]
pub struct Return {
    return_ident: String,
}

impl Statement for Return {
    fn gen_statement_asm_x86(&self) -> Vec<&'static str> {
        unimplemented!();
    }

    fn gen_statement_cxx14(&self) -> Vec<&'static str> {
        vec![format!("return {}", self.return_ident).as_str()]
    }

    fn parse(&self, initial: TokenType, iterator: TokenTypeIter<'_>) -> ParseResult {
        // Match against identifier
        let ident: String = expect!(iterator, (TokenType::Ident(val), "identifier"))?;

        // Expect terminating semicolon
        terminator!(iterator);

        Ok(Box::new(Return {
            return_ident: ident,
        }))
    }
}

/// A simple return statement without any value
#[derive(Debug)]
pub struct ReturnEmpty {}

impl Statement for ReturnEmpty {
    fn gen_statement_asm_x86(&self) -> Vec<&'static str> {
        unimplemented!();
    }

    fn gen_statement_cxx14(&self) -> Vec<&'static str> {
        vec!["return;"]
    }

    fn parse(&self, initial: TokenType, iterator: TokenTypeIter<'_>) -> ParseResult {
        // Expect terminating semicolon
        terminator!(iterator);

        Ok(Box::new(ReturnEmpty {}))
    }
}

/// A return statement returning a literal
#[derive(Debug)]
pub struct ReturnLiteral<T>
where
    T: types::Type,
{
    value: Box<T>,
}

impl<T> Statement for ReturnLiteral<T>
where
    T: types::Type,
{
    fn gen_statement_asm_x86(&self) -> Vec<&'static str> {
        unimplemented!();
    }

    fn gen_statement_cxx14(&self) -> Vec<&'static str> {
        let ret_val: &'static str = format!("return {}", self.value.as_str()).as_str();
        vec![ret_val]
    }

    fn parse(&self, initial: TokenType, iterator: TokenTypeIter<'_>) -> ParseResult {
        // Match against literal, returning an Err if it fails
        let value: String = expect!(iterator, (TokenType::Integer(val), "literal integer"))?;

        // Expect terminating semicolon
        terminator!(iterator);

        // Create literal and parse retrieved string value into i32 form
        let literal = types::Integer::new("", value.parse::<i32>().unwrap());

        Ok(Box::new(ReturnLiteral {
            value: Box::new(literal),
        }))
    }
}
