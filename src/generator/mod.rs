//! This module contains the functions and types related to the generation of output from parsed contents

use statement::Statement;

/// This function generates the appropriate x86 assembly for the vector of statements passed.
///
/// It expects a valid set of statements and will panic if an sequence is passed that it is not expecting.
///
/// # Arguments
///
/// - `statements` - A vec of boxed Statement objects of the program
///
/// # Returns
///
/// A vec of &'static str representing the x86 ASM generated
pub fn generate_x86(statements: Vec<Box<Statement>>) -> Vec<&'static str> {
    let mut source = Vec::new();

    source
}

/// This function generates the appropriate C++14 source for the vector of statements passed.
///
/// It expects a valid set of statements and will panic if an sequence is passed that it is not expecting.
///
/// # Arguments
///
/// - `statements` - A vec of boxed Statement objects of the program
///
/// # Returns
///
/// A vec of &'static str representing the C++14 source generated
pub fn generate_cxx(statements: Vec<Box<Statement>>) -> Vec<&'static str> {
    let mut source = Vec::new();

    source
}
