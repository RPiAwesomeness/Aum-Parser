extern crate aum_lexer;
extern crate aum_parser;

use aum_lexer::lexer::{Lexer, Token};
use aum_lexer::tokens::TokenType;
use aum_parser::statement::parse;

#[test]
fn parse_test() {
    let mut lexer = Lexer::new("foo := \"bar\"");
    let mut tokens: Vec<Token> = vec![];

    loop {
        let tok = lexer.next_token();
        if tok.token == TokenType::EndOfFile {
            break;
        }

        tokens.push(tok);
    }

    parse(tokens);
}
